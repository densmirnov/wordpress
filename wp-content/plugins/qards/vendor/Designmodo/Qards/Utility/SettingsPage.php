<?php
/*
 * This file is part of the Designmodo WordPress Plugin.
 *
 * (c) Designmodo Inc. <info@designmodo.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Designmodo\Qards\Utility;

/**
 * SettingsPage implements WP admin's settings page handler.
 */
class SettingsPage
{

    /**
     * Init SettingsPage
     *
     * @return void
     */
    public static function init()
    {
        add_action(
            'admin_menu',
            function () {
                add_theme_page(
                    __('Qards settings'),
                    __('Qards Settings'),
                    'edit_theme_options',
                    'qards_settings',
                    array(
                        'Designmodo\Qards\Utility\SettingsPage',
                        'show'
                    )
                );
                add_submenu_page(
                    'edit.php?post_type=' . DM_POST_TYPE,
                    __('Qards settings'),
                    __('Qards Settings'),
                    'edit_theme_options',
                    'qards_settings',
                    array(
                        'Designmodo\Qards\Utility\SettingsPage',
                        'show'
                    )
                );
            }
        );

        add_action(
            'admin_init',
            function() {

            }
        );
    }

    public static function show()
    {
        ?>

            <link rel="stylesheet" href="<?php echo DM_PLUGIN_URL; ?>custom/css/qards-settings.css" />

            <div id="qards-settings" class="invisible">
                <div class="main-content">
                    <div class="tab-content">
                        <div class="tab">
                            <div class="rows">
                                <div class="tab-row">
                                    <div class="tab-line">
                                        <div class="tab-row-cell wide">
                                            <p class="row-heading">Site Title</p>
                                            <textarea class="editable-text text-field" id="siteTitle" name="<?php echo Settings::SETTING_SITE_TITLE; ?>" placeholder="Type your title here"><?php echo esc_textarea(Settings::get(Settings::SETTING_SITE_TITLE)); ?></textarea>
                                        </div>
                                        <div class="tab-row-cell">
                                            <div class="button-holder edit">
                                                <a class="edit-button main-button" href="#">Edit</a>
                                            </div>
                                            <div class="button-holder save">
                                                <a class="save-button main-button" href="#">Save</a>
                                                <a class="cancel-button main-button" href="#">Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-row">
                                    <div class="tab-line">
                                        <div class="tab-row-cell wide">
                                            <p class="row-heading">Google Analytics API key</p>
                                            <textarea class="editable-text text-field" id="googleKey" name="<?php echo Settings::SETTING_GA_API_KEY; ?>" placeholder="Type your key here"><?php echo esc_textarea(Settings::get(Settings::SETTING_GA_API_KEY)); ?></textarea>
                                        </div>
                                        <div class="tab-row-cell">
                                            <div class="button-holder edit">
                                                <a class="edit-button main-button" href="#">Edit</a>
                                            </div>
                                            <div class="button-holder save">
                                                <a class="save-button main-button" href="#">Save</a>
                                                <a class="cancel-button main-button" href="#">Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-row">
                                    <div class="tab-line">
                                        <div class="tab-row-cell wide">
                                            <p class="row-heading"><strong>TypeKit ID</strong>. Adding fonts to your site is fast and easy. <a href="http://help.typekit.com/customer/portal/articles/6850-using-typekit-with-other-blogging-platforms" target="_blank">Learn how</a> to get your TypeKit ID.</p>
                                            <div class="typekitId-row">
                                                <textarea class="editable-text text-field" id="typeKitID" placeholder="TypeKit ID" name="<?php echo Settings::SETTING_TYPEKIT_ID; ?>"><?php echo esc_textarea(Settings::get(Settings::SETTING_TYPEKIT_ID)); ?></textarea>
                                                <p class="remove-typekitId">
                                                    <a href="#">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="12px" height="12px" fill="rgba(103,103,103,.7)" viewBox="0 0 17 16" enable-background="new 0 0 17 16"><path d="M14.3 4.5h-11.6c-.4 0-.7.3-.7.8l.7 10c0 .4.4.8.9.8h10c.4 0 .8-.3.9-.8l.7-10c-.2-.5-.5-.8-.9-.8zm-7.3 8.4c0 .5-.4.9-.9.9s-.9-.4-.9-.9v-5.5c0-.5.4-.9.9-.9s.9.4.9.9v5.5zm4.7 0c0 .5-.4.8-.8.8-.5 0-.8-.4-.8-.8v-5.6c0-.5.4-.8.8-.8.5 0 .8.4.8.8v5.6zm4.3-11.9h-4c0-.6-.4-1-1-1h-5c-.6 0-1 .4-1 1h-4c-.6 0-1 .4-1 1s.4 1 1 1h15c.6 0 1-.4 1-1s-.4-1-1-1z"></path></svg>
                                                    </a>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="tab-row-cell">
                                            <div class="button-holder edit">
                                                <a class="edit-button main-button" href="#">Edit</a>
                                            </div>
                                            <div class="button-holder save">
                                                <a class="save-button main-button" href="#">Save</a>
                                                <a class="cancel-button main-button" href="#">Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-row no-image">
                                    <div class="tab-line">
                                        <div class="tab-row-cell wide">
                                            <p class="row-heading"><strong>Upload logo</strong>. Use <i>.png</i>, <i>.jpg</i> or <i>.gif</i>.
                                                <br>Maximum file size of 30KB</p>
                                            <span class="upload-image main-button">Browse<input id="logo" data-file-size="30000" class="fileupload" type="file" accept="image/gif, image/jpeg, image/pjpeg, image/png" name="<?php echo Settings::SETTING_LOGO; ?>" /></span>
                                            <a class="remove-image main-button" href="#">Remove</a>
                                        </div>
                                        <div class="tab-row-cell">
                                            <div class="logo-preview">
                                                <img class="preview-img" data-preview-src="<?php echo Settings::get(Settings::SETTING_LOGO);?>" src="<?php echo DM_PLUGIN_URL . Settings::get(Settings::SETTING_LOGO);?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-row">
                                    <div class="tab-line">
                                        <div class="tab-row-cell wide">
                                            <p class="row-heading">Check this box if you want to help us <strong>promote Qards</strong> on your pages.</p>
                                        </div>
                                        <div class="tab-row-cell">
                                            <label class="custom-checkbox">
                                                <input id="promoteQards" class="checkbox" type="checkbox" name="<?php echo Settings::SETTING_SHOW_PROMO_BANNER; ?>" <?php if (Settings::get(Settings::SETTING_SHOW_PROMO_BANNER)) { echo ' checked="checked" '; } ?> />
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-row">
                                    <div class="tab-line">
                                        <div class="tab-row-cell wide">
                                            <p class="row-heading"><strong>Set global CSS</strong> if you care about it. This code will be used on each page, so be careful with it.</p>
                                        </div>
                                        <div class="tab-row-cell">
                                            <div class="button-holder edit">
                                                <a class="edit-button main-button" href="#">Edit</a>
                                            </div>
                                            <div class="button-holder save">
                                                <a class="save-button main-button" href="#">Save</a>
                                                <a class="cancel-button main-button" href="#">Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="code-editor">
                                        <textarea class="code-editor-textarea text-field" data-code-mode="css" id="editingCSS" name="<?php echo Settings::SETTING_GLOBAL_CSS;?>"><?php echo esc_textarea(Settings::get(Settings::SETTING_GLOBAL_CSS)); ?></textarea>
                                    </div>
                                </div>
                                <div class="tab-row">
                                    <div class="tab-line">
                                        <div class="tab-row-cell wide">
                                            <p class="row-heading"><strong>Add Code to Head</strong>. This code will be added to the <i>&lt;head&gt;</i> section of every page.</p>
                                        </div>
                                        <div class="tab-row-cell">
                                            <div class="tab-row-cell">
                                                <div class="button-holder edit">
                                                    <a class="edit-button main-button" href="#">Edit</a>
                                                </div>
                                                <div class="button-holder save">
                                                    <a class="save-button main-button" href="#">Save</a>
                                                    <a class="cancel-button main-button" href="#">Cancel</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="code-editor">
                                        <textarea class="code-editor-textarea text-field" data-code-mode="text/html" id="editingHead" name="<?php echo Settings::SETTING_WP_HEAD;?>"><?php echo esc_textarea(Settings::get(Settings::SETTING_WP_HEAD));?></textarea>
                                    </div>
                                </div>
                                <div class="tab-row">
                                    <div class="tab-line">
                                        <div class="tab-row-cell wide">
                                            <p class="row-heading"><strong>Set Header</strong>. This code will be added on every page after the <i>&lt;body&gt;</i> tag.</p>
                                        </div>
                                        <div class="tab-row-cell">
                                            <div class="button-holder edit">
                                                <a class="edit-button main-button" href="#">Edit</a>
                                            </div>
                                            <div class="button-holder save">
                                                <a class="save-button main-button" href="#">Save</a>
                                                <a class="cancel-button main-button" href="#">Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="code-editor">
                                        <textarea class="code-editor-textarea text-field" data-code-mode="text/html" id="editingHeader" name="<?php echo Settings::SETTING_WP_HEADER;?>"><?php echo esc_textarea(Settings::get(Settings::SETTING_WP_HEADER));?></textarea>
                                    </div>
                                </div>
                                <div class="tab-row">
                                    <div class="tab-line">
                                        <div class="tab-row-cell wide">
                                            <p class="row-heading"><strong>Set Footer</strong>. This code will be added before the end of the page.</p>
                                        </div>
                                        <div class="tab-row-cell">
                                            <div class="button-holder edit">
                                                <a class="edit-button main-button" href="#">Edit</a>
                                            </div>
                                            <div class="button-holder save">
                                                <a class="save-button main-button" href="#">Save</a>
                                                <a class="cancel-button main-button" href="#">Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="code-editor">
                                        <textarea class="code-editor-textarea text-field" data-code-mode="text/html" id="editingFooter" name="<?php echo Settings::SETTING_WP_FOOTER;?>"><?php echo esc_textarea(Settings::get(Settings::SETTING_WP_FOOTER));?></textarea>
                                    </div>
                                </div>
                                <div class="tab-row">
                                    <div class="tab-line">
                                        <div class="tab-row-cell wide">
                                            <p class="row-heading">If you use the Subscribe component you can <strong>export your subscribers</strong> in a .csv format.</p>
                                        </div>
                                        <div class="tab-row-cell">
                                            <div class="button-holder">
                                                <a class="main-button" id="exportSubscribers" href="#">Export</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-row">
                                    <div class="tab-line">
                                        <div class="tab-row-cell wide">
                                            <p class="row-heading">If you think you have made a mistake with the settings, be sure to reset it.</p>
                                        </div>
                                        <div class="tab-row-cell">
                                            <div class="button-holder">
                                                <a class="main-button reset-button" id="resetSettings" href="#">Reset Settings</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <script>
                var pluginUrlAdmin = "<?php echo DM_PLUGIN_URL; ?>";
            </script>

            <script src="<?php echo DM_PLUGIN_URL; ?>custom/js/vendor/file-upload/jquery.ui.widget.js"></script>
            <script src="<?php echo DM_PLUGIN_URL; ?>custom/js/vendor/file-upload/jquery.iframe-transport.js"></script>
            <script src="<?php echo DM_PLUGIN_URL; ?>custom/js/vendor/file-upload/jquery.fileupload.js"></script>

            <script src="<?php echo DM_PLUGIN_URL; ?>custom/js/vendor/flex-text/jquery.flexText.min.js"></script>

            <script src="<?php echo DM_PLUGIN_URL; ?>custom/js/vendor/codemirror/codemirror.js"></script>
            <script src="<?php echo DM_PLUGIN_URL; ?>custom/js/vendor/codemirror/css.js"></script>
            <script src="<?php echo DM_PLUGIN_URL; ?>custom/js/vendor/codemirror/xml.js"></script>
            <script src="<?php echo DM_PLUGIN_URL; ?>custom/js/vendor/codemirror/search.js"></script>
            <script src="<?php echo DM_PLUGIN_URL; ?>custom/js/vendor/codemirror/searchcursor.js"></script>
            <script src="<?php echo DM_PLUGIN_URL; ?>custom/js/vendor/codemirror/dialog.js"></script>
            <script src="<?php echo DM_PLUGIN_URL; ?>custom/js/vendor/codemirror/beautifier.js"></script>
            <script src="<?php echo DM_PLUGIN_URL; ?>custom/js/vendor/codemirror/simplescrollbars.js"></script>

            <script src="<?php echo DM_PLUGIN_URL; ?>custom/js/qards-settings.js"></script>

        <?php
    }
}
